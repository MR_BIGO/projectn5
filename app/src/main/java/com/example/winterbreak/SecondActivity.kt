package com.example.winterbreak

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }


    val name = NameEditText.text.toString()
    val lastname = LastnameEditText.text.toString()
    val age = ageEditText.text.toString()

    private fun init() {
        ProfilePageButton.setOnClickListener {
            if (name.isNotEmpty() && lastname.isNotEmpty() && age.isNotEmpty()) {
                val intent = Intent(this, ThirdActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Enter your information", Toast.LENGTH_SHORT).show()
            }
        }

    }
}