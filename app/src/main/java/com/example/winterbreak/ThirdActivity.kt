package com.example.winterbreak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_third.*

class ThirdActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)
        init()
    }

    private fun init() {

        val name = intent.extras?.getString("first_name", "")
        val lastname = intent.extras?.getString("last_name", "")
        val age = intent.extras?.getInt("age", 0)


        first_name.text = name
        last_name.text = lastname
        ageTextView.text = age.toString()


    }


}