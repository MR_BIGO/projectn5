package com.example.winterbreak

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_first.*


class FirstActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
    }

    val password = passwordTextView.text.toString()
    val email = emailTextView.text.toString()

    private fun init() {
        signInButton.setOnClickListener {
            if (password.isNotEmpty() && email.isNotEmpty()) {
                val intent = Intent(this, SecondActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Your Email or Password is not submitted.", Toast.LENGTH_SHORT)
                    .show()

            }

        }

    }

}